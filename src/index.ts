import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import { getTimestamp } from './utils';

dotenv.config();

export const app: Express = express();
export const port = process.env.PORT;

app.get('/', (req: Request, res: Response) => {
  res.send(`express + Typescript server. Timestamp: ${getTimestamp()}`);
});

const startServer = async () => {
  app.listen(port, () => {
    console.log(`[server]: Server is running at http://localhost:${port}`);
  });
};

startServer();
