import { getTimestamp } from './utils';

describe('Utils', () => {
  test('getTimestamp returns a valid timestamp', () => {
    const getTime = () => {
      return [`${new Date()}`, getTimestamp()];
    };
    const time = getTime();
    expect(time[0]).toBe(time[1]);
  });
});
